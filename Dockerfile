FROM alpine

ARG POSTGRES_USER
ARG POSTGRES_PASSWORD
ARG DB_HOST_NAME
ARG POSTGRES_DB

RUN apk --no-cache add postgresql-client

COPY ["create.sql", "."]

RUN psql postgresql://$POSTGRES_USER:$POSTGRES_PASSWORD@$DB_HOST_NAME:5432/$POSTGRES_DB -f create.sql

ENTRYPOINT ["tail -F rebuild-me-if-sql-file-changes"]
